NAME = ckati
SOURCES = affinity.cc \
          command.cc \
          dep.cc \
          eval.cc \
          exec.cc \
          expr.cc \
          file.cc \
          file_cache.cc \
          fileutil.cc \
          find.cc \
          flags.cc \
          func.cc \
          io.cc \
          log.cc \
	  main.cc \
          ninja.cc \
          parser.cc \
          regen.cc \
          rule.cc \
          stats.cc \
          stmt.cc \
          string_piece.cc \
          stringprintf.cc \
          strutil.cc \
          symtab.cc \
          thread_pool.cc \
          timeutil.cc \
          var.cc \
          version_unknown.cc
CXXFLAGS += -DNOLOG
LDFLAGS += -lrt -lpthread

build: $(SOURCES)
	$(CXX) $^ -o $(NAME) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)